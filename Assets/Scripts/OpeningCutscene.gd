extends Node2D

var timer = null
var scene1_0 = null
var scene1_1 = null
var scene1_2 = null
var scene1_3 = null
var scene1_4 = null
var scene1_5 = null
var scene1_6 = null

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		var dir = Directory.new()
		dir.remove("res://tempinfo.dump")
		get_tree().quit()

func _ready():
	
	# associate child nodes with variables for ease of use later
	scene1_0 = get_node("scene1_0")
	scene1_1 = get_node("scene1_1")
	scene1_2 = get_node("scene1_2")
	scene1_3 = get_node("scene1_3")
	scene1_4 = get_node("scene1_4")
	scene1_5 = get_node("scene1_5")
	timer = get_node("Timer")
	
	# set a timer to wait between showing images for the cutscene.
	timer.set_wait_time(2.0)
	timer.connect("timeout", self, "show_next_scene")
	
	scene1_0.show()
	
	timer.start()

func show_next_scene():
	if scene1_0.is_visible():
		scene1_1.show() # show the next one first, so there isn't a gap where there is nothing on screen.
		scene1_0.hide()
		timer.set_wait_time(4.0)
		timer.start()
	elif scene1_1.is_visible():
		scene1_2.show()
		scene1_1.hide()
		timer.start()
	elif scene1_2.is_visible():
		scene1_3.show()
		scene1_2.hide()
		timer.start()
	elif scene1_3.is_visible():
		scene1_4.show()
		scene1_3.hide()
		timer.set_wait_time(5.0)
		timer.start()
	elif scene1_4.is_visible():
		scene1_5.show()
		scene1_4.hide()
		get_node("AudioStreamPlayer").play()
		timer.set_wait_time(3.0)
		timer.start()
	elif scene1_5.is_visible():
		get_tree().change_scene("res://Assets/Scenes/Level1.tscn")