extends Node2D

var pause_menu = load("res://Assets/Scenes/PauseMenu.tscn")
var pause_menu_node

var scene_num = 0

var player

var scene_timer
var terminal_1_timer
var terminal_2_timer
var terminal_3_timer
var terminal_4_timer
var terminal_5_timer
var terminal_6_timer

var player_touching_door = false

var player_touching_terminal_1
var terminal_1_locked = true
var terminal_1_activated = false

var player_touching_terminal_2
var terminal_2_activated = false

var player_touching_terminal_3
var terminal_3_locked = true
var terminal_3_activated = false

var player_touching_terminal_4
var terminal_4_locked = true
var terminal_4_activated = false

var player_touching_terminal_5
var terminal_5_activated = false

var player_touching_terminal_6
var terminal_6_locked = true
var terminal_6_activated = false

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		var dir = Directory.new()
		dir.remove("res://tempinfo.dump")
		get_tree().quit()

func _ready():
	scene_timer = get_node("Scene Timer")
	terminal_1_timer = get_node("Terminal 1 Timer")
	terminal_2_timer = get_node("Terminal 2 Timer")
	terminal_3_timer = get_node("Terminal 3 Timer")
	terminal_4_timer = get_node("Terminal 4 Timer")
	terminal_5_timer = get_node("Terminal 5 Timer")
	terminal_6_timer = get_node("Terminal 6 Timer")
	
	player = get_node("Player")
	
	var loaded_data = {}
	var file = File.new()
	file.open("res://savegame.save", File.READ)
	loaded_data = parse_json(file.get_line())
	get_node("Dramatic Music").play(loaded_data.music_pos)
	player.chosen_player = loaded_data.chosen_player
	player.player_lives = loaded_data.player_lives
	
	player.player_can_move = false
	
	scene_timer.set_wait_time(1.0)
	scene_timer.start()
	
	$"Level Section 1".show()
	$"Level Section 2".hide()
	$"Barrier 1".show()
	$"Barrier 2".show()
	$"Barrier 3".show()
	$"Barrier 4".show()
	$"Barrier 5".show()
	$"Jump Pad 1".hide()
	$"Jump Pad 2".hide()
	$"Jump Pad 3".hide()
	$"Jump Pad 4".hide()
	$"Jump Pad 5".hide()
	$"Jump Pad 6".hide()
	$"Jump Pad 7".hide()
	$"Jump Pad 8".hide()
	$"Key 1".hide()
	$"Key 2".hide()
	$"Key 3".hide()
	$"Key 4".hide()
	$"Terminal 3".hide()
	$"Terminal 4".hide()
	$"Terminal 5".hide()
	$"Terminal 6".hide()
	$"Exit Door".hide()

func _process(delta):
	if not get_node("Dramatic Music").is_playing():
		get_node("Dramatic Music").play()
	if player.player_lives <= 4:
		get_node("Player/Camera2D/Heart 5").hide()
	if player.player_lives <= 3:
		get_node("Player/Camera2D/Heart 4").hide()
	if player.player_lives <= 2:
		get_node("Player/Camera2D/Heart 3").hide()
	if player.player_lives <= 1:
		get_node("Player/Camera2D/Heart 2").hide()
	if player.player_lives == 0:
		get_tree().change_scene("res://Assets/Scenes/GameOver.tscn")
	if not player_touching_terminal_1:
		terminal_1_timer.stop()
	if not player_touching_terminal_2:
		terminal_2_timer.stop()
	if not player_touching_terminal_3:
		terminal_3_timer.stop()
	if not player_touching_terminal_4:
		terminal_4_timer.stop()
	if not player_touching_terminal_5:
		terminal_5_timer.stop()
	if not player_touching_terminal_6:
		terminal_6_timer.stop()

func _input(event):
	if event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if event.is_action_pressed("ui_pause"):
		if $Player/Camera2D.is_current():
			pause_menu_node = pause_menu.instance()
			add_child(pause_menu_node)
			$"Pause Menu".position = $Player.position - Vector2(512, 300)
			get_tree().paused = true
	if event.is_action_pressed("use"):
		if player_touching_terminal_1 and terminal_1_activated == false and terminal_1_locked == false:
			terminal_1_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("5_sec")
		if player_touching_terminal_2 and terminal_2_activated == false:
			terminal_2_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("10_sec")
		if player_touching_terminal_3 and terminal_3_activated == false and terminal_3_locked == false:
			terminal_3_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("5_sec")
		if player_touching_terminal_4 and terminal_4_activated == false and terminal_4_locked == false:
			terminal_4_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("10_sec")
		if player_touching_terminal_5 and terminal_5_activated == false:
			terminal_5_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("5_sec")
		if player_touching_terminal_6 and terminal_6_activated == false and terminal_6_locked ==false:
			terminal_6_timer.start()
			$"Player/Terminal Progress Bar".show()
			$"Player/Terminal Progress Bar".play("10_sec")
		if player_touching_door:
			var save_data = {
				music_pos = get_node("Dramatic Music").get_playback_position(),
				current_level = 4,
				chosen_player = player.chosen_player,
				player_lives = player.player_lives
				}
			var file = File.new()
			file.open("res://savegame.save", File.WRITE)
			file.store_line(to_json(save_data))
			file.close()
			get_tree().change_scene("res://Assets/Scenes/Cutscene4.tscn")
	elif event.is_action_released("use"):
		terminal_1_timer.stop()
		terminal_2_timer.stop()
		terminal_3_timer.stop()
		terminal_4_timer.stop()
		terminal_5_timer.stop()
		terminal_6_timer.stop()
		$"Player/Terminal Progress Bar".hide()
		$"Player/Terminal Progress Bar".play("idle")

func _on_Scene_Timer_timeout():
	if scene_num == 0:
		$Crandley/Camera2D.make_current()
		$"Crandley/Speech 1".show()
		scene_timer.set_wait_time(3.0)
		scene_timer.start()
		scene_num += 1
	elif scene_num == 1:
		$"Crandley/Speech 1".hide()
		$Player/Camera2D.make_current()
		$"Player/Speech 1".show()
		scene_timer.set_wait_time(2.0)
		scene_timer.start()
		scene_num += 1
	elif scene_num == 2:
		$"Player/Speech 1".hide()
		player.player_can_move = true
		scene_timer.stop()

func _on_Jump_Pad_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player.jump_power = 400
		$"Jump Pad 1/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 1/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_2_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player.jump_power = 400
		$"Jump Pad 2/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_2_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 2/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_3_Activation_Area_body_entered(body):
	if body.get_name() == "Player" and terminal_5_activated:
		player.jump_power = 400
		$"Jump Pad 3/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_3_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 3/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_4_Activation_Area_body_entered(body):
	if body.get_name() == "Player" and terminal_5_activated:
		player.jump_power = 400
		$"Jump Pad 4/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_4_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 4/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_5_Activation_Area_body_entered(body):
	if body.get_name() == "Player" and terminal_5_activated:
		player.jump_power = 400
		$"Jump Pad 5/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_5_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 5/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_6_Activation_Area_body_entered(body):
	if body.get_name() == "Player" and terminal_5_activated:
		player.jump_power = 500
		$"Jump Pad 6/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_6_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 6/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_7_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player.jump_power = 400
		$"Jump Pad 7/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_7_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 7/AnimatedSprite".set_animation("unactivated")

func _on_Jump_Pad_8_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player.jump_power = 500
		$"Jump Pad 8/AnimatedSprite".set_animation("activated")

func _on_Jump_Pad_8_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player.jump_power = 150
		$"Jump Pad 8/AnimatedSprite".set_animation("unactivated")

func _on_Key_1_Pickup_Area_body_entered(body):
	if body.get_name() == "Player":
		terminal_1_locked = false
		$"Terminal 1/AnimatedSprite".set_animation("unactivated")
		$"Key 1".queue_free()

func _on_Key_2_Pickup_Area_body_entered(body):
	if body.get_name() == "Player":
		terminal_4_locked = false
		$"Terminal 4/AnimatedSprite".set_animation("unactivated")
		$"Key 2".queue_free()

func _on_Key_3_Pickup_Area_body_entered(body):
	if body.get_name() == "Player":
		terminal_3_locked = false
		$"Terminal 3/AnimatedSprite".set_animation("unactivated")
		$"Key 3".queue_free()

func _on_Key_4_Pickup_Area_body_entered(body):
	if body.get_name() == "Player" and terminal_3_activated == true:
		terminal_6_locked = false
		$"Terminal 6/AnimatedSprite".set_animation("unactivated")
		$"Jump Pad 7".queue_free()
		$"Jump Pad 8".queue_free()
		$"Key 4".queue_free()

func _on_Terminal_1_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_1 = true

func _on_Terminal_1_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_1 = false

func _on_Terminal_2_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_2 = true

func _on_Terminal_2_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_2 = false

func _on_Terminal_3_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_3 = true

func _on_Terminal_3_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player_touching_terminal_3 = false

func _on_Terminal_4_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_4 = true

func _on_Terminal_4_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_4 = false

func _on_Terminal_5_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_5 = true

func _on_Terminal_5_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_5 = false

func _on_Terminal_6_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_6 = true

func _on_Terminal_6_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_6 = false

func _on_Terminal_1_Timer_timeout():
	terminal_1_activated = true
	terminal_1_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 1/AnimatedSprite".play("activated")
	
	$"Level Section 2".show()
	$"Barrier 2".queue_free()
	$"Key 2".show()
	$"Jump Pad 1".show()
	$"Jump Pad 2".show()
	$"Terminal 3".show()
	$"Terminal 4".show()

func _on_Terminal_2_Timer_timeout():
	terminal_2_activated = true
	terminal_2_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 2/AnimatedSprite".play("activated")
	
	$"Barrier 1".queue_free()
	$"Key 1".show()

func _on_Terminal_3_Timer_timeout():
	terminal_3_activated = true
	terminal_3_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 3/AnimatedSprite".play("activated")
	
	$"Key 4".show()
	$"Barrier 4".queue_free()
	$"Terminal 6".show()
	$"Jump Pad 7".show()
	$"Jump Pad 8".show()

func _on_Terminal_4_Timer_timeout():
	terminal_4_activated = true
	terminal_4_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 4/AnimatedSprite".play("activated")
	
	$"Key 3".show()
	$"Terminal 5".show()
	$"Barrier 3".queue_free()

func _on_Terminal_5_Timer_timeout():
	terminal_5_activated = true
	terminal_5_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 5/AnimatedSprite".play("activated")
	
	$"Jump Pad 3".show()
	$"Jump Pad 4".show()
	$"Jump Pad 5".show()
	$"Jump Pad 6".show()

func _on_Terminal_6_Timer_timeout():
	terminal_6_activated = true
	terminal_6_timer.stop()
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	$"Terminal 6/AnimatedSprite".play("activated")
	
	$"Barrier 5".queue_free()
	$"Exit Door".show()

func _on_Exit_Door_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_door = true


func _on_Exit_Door_Area_body_exited(body):
	if body.get_name() == "Player":
		player_touching_door = false