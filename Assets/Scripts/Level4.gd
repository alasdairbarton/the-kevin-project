extends Node2D

var player_starting_pos = Vector2(940, 40)

var player

var player_is_touching_door

var player_has_seen_note = false
var is_touching_note

var has_unlocked_exit_terminal = false

var player_touching_exit_terminal
var exit_terminal_locked = true
var exit_terminal_activated = false
var exit_terminal_timer

var scene_num = 0
var player_has_shown_speech_2 = false
var player_has_shown_speech_3 = false

var pause_menu = load("res://Assets/Scenes/PauseMenu.tscn")
var pause_menu_node

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		var dir = Directory.new()
		dir.remove("res://tempinfo.dump")
		get_tree().quit()

func _physics_process(delta):
	if player.position.y >= $"Death Position Y".position.y:
		player.player_is_dead = true

func _process(delta):
	if not get_node("Dramatic Music").is_playing():
		get_node("Dramatic Music").play()
	if player.player_is_dead:
		player.player_lives -= 1
		player.position = player_starting_pos
		player.player_is_dead = false
	if player.player_lives <= 4:
		get_node("Player/Camera2D/Heart 5").hide()
	if player.player_lives <= 3:
		get_node("Player/Camera2D/Heart 4").hide()
	if player.player_lives <= 2:
		get_node("Player/Camera2D/Heart 3").hide()
	if player.player_lives <= 1:
		get_node("Player/Camera2D/Heart 2").hide()
	if player.player_lives == 0:
		get_tree().change_scene("res://Assets/Scenes/GameOver.tscn")
	
	if $"Terminal Group 1".has_right_combination:
		if $"Terminal Group 2".has_right_combination:
			if $"Terminal Group 3".has_right_combination:
				if $"Terminal Group 4".has_right_combination:
					if player_has_seen_note:
						exit_terminal_locked = false
						if not has_unlocked_exit_terminal:
							$"Exit Terminal/AnimatedSprite".play("unactivated")
							has_unlocked_exit_terminal = true

func _ready():
	player = get_node("Player")
	
	var loaded_data = {}
	var file = File.new()
	file.open("res://savegame.save", File.READ)
	loaded_data = parse_json(file.get_line())
	get_node("Dramatic Music").play(loaded_data.music_pos)
	player.chosen_player = loaded_data.chosen_player
	player.player_lives = loaded_data.player_lives
	
	player.player_can_move = false
	
	exit_terminal_timer = get_node("Exit Terminal Timer")
	$"Exit Terminal/AnimatedSprite".play()

func _input(event):
	if event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if event.is_action_pressed("ui_pause"):
		if $Player/Camera2D.is_current():
			pause_menu_node = pause_menu.instance()
			add_child(pause_menu_node)
			$"Pause Menu".position = $Player.position - Vector2(512, 300)
			get_tree().paused = true
	if event.is_action_pressed("use"):
		if player_touching_exit_terminal and not exit_terminal_activated and not exit_terminal_locked:
			exit_terminal_timer.start()
			get_node("Player/Terminal Progress Bar").show()
			get_node("Player/Terminal Progress Bar").play("20_sec")
		if is_touching_note:
			$Player/note.show()
			player_has_seen_note = true
		if player_is_touching_door and exit_terminal_activated:
			var save_data = {
				music_pos = get_node("Dramatic Music").get_playback_position(),
				current_level = 5,
				chosen_player = player.chosen_player,
				player_lives = player.player_lives
				}
			var file = File.new()
			file.open("res://savegame.save", File.WRITE)
			file.store_line(to_json(save_data))
			file.close()
			get_tree().change_scene("res://Assets/Scenes/Level5.tscn")
	elif event.is_action_released("use"):
		exit_terminal_timer.stop()
		get_node("Player/Terminal Progress Bar").hide()
		get_node("Player/Terminal Progress Bar").play("idle")
		$Player/note.hide()

func _on_Bouncy_Stuff_Detector_body_entered(body):
	if body.get_name() == "Bouncy Stuff":
		player.jump_power = 500
		if not player_has_shown_speech_2:
			$Player/speech_2.show()
			$"Scene Timer".start()
			player_has_shown_speech_2 = true

func _on_Bouncy_Stuff_Detector_body_exited(body):
	if body.get_name() == "Bouncy Stuff":
		player.jump_power = 150

func _on_Scene_Timer_timeout():
	if scene_num == 0:
		$Jackson/speech_1.show()
		scene_num += 1
	elif scene_num == 1:
		$Jackson/speech_1.hide()
		$Player/speech_1.show()
		scene_num += 1
	elif scene_num == 2:
		$Player/speech_1.hide()
		player.player_can_move = true
		$"Scene Timer".stop()
		scene_num += 1
	elif scene_num == 3:
		$Player/speech_2.hide()
		scene_num += 1
		$"Scene Timer".stop()
	elif scene_num == 4:
		$Player/speech_3.hide()
		scene_num += 1
		$"Scene Timer".stop()

func _on_Exit_Terminal_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_exit_terminal = true

func _on_Exit_Terminal_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player_touching_exit_terminal = false

func _on_Exit_Terminal_Timer_timeout():
	exit_terminal_activated = true
	$"Exit Terminal/AnimatedSprite".play("activated")
	$"Player/Terminal Progress Bar".hide()
	$"Player/Terminal Progress Bar".play("idle")
	
	$"Exit Door".show()

func _on_Pinned_Note_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		if not player_has_shown_speech_3:
			$Player/speech_3.show()
			$"Scene Timer".start()
			player_has_shown_speech_3 = true
		is_touching_note = true

func _on_Pinned_Note_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		is_touching_note = false

func _on_Door_Exit_Area_body_entered(body):
	player_is_touching_door = true

func _on_Door_Exit_Area_body_exited(body):
	player_is_touching_door = false